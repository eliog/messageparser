﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ConsoleApplication1.Model;
using Ciloci.Flee;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            var customer1 = CreateTestData();

            //--- test out expression parser
            var parser = new ExpressionParser(customer1.orders[0]);
            Console.WriteLine( parser.Evaluate("Customer.Name"));
            Console.WriteLine( parser.Evaluate("OrderDate.AddDays(7)"));
            Console.WriteLine(parser.Evaluate("DateTime.Today.AddDays(7)"));
            // Try some math
            Console.WriteLine(parser.Evaluate("Details[0].Total * .07m"));
            //--- test out message parser
            string test = @"You have until {OrderDate.AddDays(7)} to Process Order For {Customer.Name}";
            Console.WriteLine(parser.ProcessMessage(test));
            
            // try under different object
            parser = new ExpressionParser(customer1);
            Console.WriteLine(parser.ProcessMessage("Hello {Name} You have {Orders.Count} Orders"));

            System.Console.ReadLine();
        }

        private static Customer CreateTestData()
        {
            // makeup some test data
            var product1 = new Product() { id = 1, Description = "Product 1", Price = 200.01m, QtyOnHand = 21 };
            var product2 = new Product() { id = 2, Description = "Product 2", Price = 200.02m, QtyOnHand = 22 };
            var product3 = new Product() { id = 3, Description = "Product 3", Price = 200.03m, QtyOnHand = 23 };

            var customer1 = new Customer()
            {
                Name = "Mr. Burns",
                orders = new List<Order>() {
                    new Order() { Number = "ORDER1" , OrderDate = DateTime.Now,
                        details = new List<OrderDetail>() 
                        {
                            new OrderDetail() { Product = product1, Description ="OrderLineItem 1", Price = 200.01m, Qty = 2},
                            new OrderDetail() { Product = product2, Description ="OrderLineItem 2", Price = 200.02m, Qty = 2 },
                            new OrderDetail() { Product = product3, Description ="OrderLineItem 3", Price = 200.03m, Qty = 2 }
                        }
                    }
                }
            };
            customer1.orders[0].Customer = customer1;
            
            return customer1;
        }
    }
}
