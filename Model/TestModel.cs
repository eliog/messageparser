﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication1.Model
{
    public class Customer
    {
        public string Name { get; set; }
        public List<Order> orders { get; set; }
    }

    public class Order
    {
        public string Number { get; set; }
        public Customer Customer { get; set; }
        public List<OrderDetail> details { get; set; }
        public DateTime OrderDate { get; set; }
    }

    public class OrderDetail
    {
        public string Description { get; set; }
        public int Qty { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get { return Qty * Price;  } }
        public Product Product { get; set; }
    }

    public class Product

    {
        public int id { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int QtyOnHand { get; set; }
    }
}
