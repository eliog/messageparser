﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ciloci.Flee;
using System.Text.RegularExpressions;

namespace ConsoleApplication1
{
    public class ExpressionParser
    {
        object _owner;
        public ExpressionParser(object owner)
        {
            _owner = owner;
        }

        public string Evaluate(string expression)
        {
           
            var context = new ExpressionContext(_owner);
            context.Options.OwnerMemberAccess = System.Reflection.BindingFlags.Public;
            context.Imports.AddType(typeof(DateTime), "DateTime");
            var e = context.CompileDynamic(expression);
            return e.Evaluate().ToString();
        }

        public string ProcessMessage(string message)
        {
            var r = new Regex(@"({[^}]+})");
            MatchCollection matches = r.Matches(message);
            List<string> expressions = new List<string>();

            foreach (Match match in matches)
            {
                expressions.Add(match.Groups[1].Value);  
            }
            foreach (var expression in expressions) {
                var stripped = expression.Substring(1, expression.Length - 2); // strip out {  and } 
                message=message.Replace(expression, Evaluate(stripped));
            }
            return message;
        }

    }
}
